#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <gsl/gsl_sf_lambert.h>         //inverse of maxwellian

//variables defining ranges
#define e_ranges     2000               //Max. number of bins for energy
#define x_ranges     30                 //Max. number of bins for pos
#define max_x        0.5               //Maximum value for positions (m)
#define t_ranges     30
#define mu_ranges    20                 //Max. number of bins for mu

//cross sections
#define c_ranges     19901
#define d_epsilon1   0.001              //Energy difference between cross
                                        //section values till 10eV
#define d_epsilon2   0.1                //same but above 10ev
#define processes    3                  //Number of processes
#define ar_mass      39.948*1.66e-27     //Ar mass in SI units
#define ar_ion_level 15.8               //Threshold energy for ionization
#define ar_exc       11.5               //Threshold energy for excitation


//physical constants
#define k_boltz      1.38e-23           //Boltzmann constant in SI units
#define e_charge     1.602e-19          //Electron charge in SI units
#define e_mass       9.1e-31            //Mass of electron in SI units
#define pi           M_PI


/* Global variables */
double DT_MC;                           //Monte Carlo time step
double e_field;                         //Electric field
int no_of_e;                            //Current number of electrons
char run[256];                          //run name
double time_simul = 4e-7;               //Simulation time

//working conditions ( SI units )
double ar_density;
double pressure = 100.0;
double temperature = 300.0;
double reduced_field = 200.0*1e-21;  
double mean_energy=0;
double temp_electron=0;

/* cross sections and collision frequencies */
double *sigma_ar_mom, *sigma_ar_ion, *sigma_ar_exc, *sigma_tot;
double *nu_ar_mom, *nu_ar_ion, *nu_ar_exc, *nu_tot, nu_star;

/**********************/
/* Initialize a state */
/**********************/
typedef struct init_state {
  double x0;               // Initial position     
  double e0;               // Initial energy           
  double m0;               // m0 = cos(theta) with theta the angle with x 
  double t0;               // Time
  struct init_state *next; // pointer to this init_state
                           // in put and get_particle will point to next particle 
} init_state;


/* Local variables for collision: */
typedef struct LOC_collision {
  double *x, *e, *m, *t;
} LOC_collision;

init_state *e_list;   // list of electrons              
init_state *f_list;   // list of time-out electrons   


/* outputs */
int**** eedf;                            //electron distribution
int total_num_e=0;
double proc[processes][x_ranges];        //keeper of processes
double proc_ion_summed;
double proc_mom_summed;
double proc_exc_summed;
double power_loss[processes];            //energy loss by each process
double k_ion=0;
double k_exc=0;
double k_mom=0;
double v_drift_summed=0;
double v_drift_aux[x_ranges];  
double t_drift[x_ranges];                //Drift time
double townsend_c[x_ranges];             //Townsend coefficient
double townsend_c_summed;     

/**************************************************************************************/
/* Fast random number generator                                                       */
/* taken from:                                                                        */
/*http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/CODES/MTARCOK/mt19937ar-cok.c*/
/**************************************************************************************/
/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UMASK 0x80000000UL /* most significant w-r bits */
#define LMASK 0x7fffffffUL /* least significant r bits */
#define MIXBITS(u,v) ( ((u) & UMASK) | ((v) & LMASK) )
#define TWIST(u,v) ((MIXBITS(u,v) >> 1) ^ ((v)&1UL ? MATRIX_A : 0UL))

static unsigned long state[N]; /* the array for the state vector  */
static int left = 1;
static int initf = 0;
static unsigned long *next;

/* initializes state[N] with a seed */
void init_genrand(unsigned long s)
{
  int j;
  state[0]= s & 0xffffffffUL;
  for (j=1; j<N; j++) {
    state[j] = (1812433253UL * (state[j-1] ^ (state[j-1] >> 30)) + j); 
    /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
    /* In the previous versions, MSBs of the seed affect   */
    /* only MSBs of the array state[].                     */
    /* 2002/01/09 modified by Makoto Matsumoto             */
    state[j] &= 0xffffffffUL;  /* for >32 bit machines */
  }
  left = 1; initf = 1;
}
static void next_state(void)
{
  unsigned long *p=state;
  int j;

  /* if init_genrand() has not been called, */
  /* a default initial seed is used         */
  if (initf==0) init_genrand(5489UL);

  left = N;
  next = state;

  for (j=N-M+1; --j; p++) 
    *p = p[M] ^ TWIST(p[0], p[1]);

  for (j=M; --j; p++) 
    *p = p[M-N] ^ TWIST(p[0], p[1]);

  *p = p[M-N] ^ TWIST(p[0], state[0]);
}
/* generates a random number on [0,1]-real-interval */
double genrand_real1(void)
{
  unsigned long y;

  if (--left == 0) next_state();
  y = *next++;

  /* Tempering */
  y ^= (y >> 11);
  y ^= (y << 7) & 0x9d2c5680UL;
  y ^= (y << 15) & 0xefc60000UL;
  y ^= (y >> 18);

  return (double)y * (1.0/4294967295.0); 
  /* divided by 2^32-1 */ 
}

/****************************************************/
/* cross section input and conversion to macroscopic*/
/* computation of collision frequencies             */
/****************************************************/
void conv_macroscopic()
{
  int k;
  double v;

  printf("Convertion to macroscopic\n");

  nu_star=0; // constant collision frequency for null collision

  nu_tot = malloc(sizeof(double)*(c_ranges));
  nu_ar_mom = malloc(sizeof(double)*(c_ranges));
  nu_ar_ion = malloc(sizeof(double)*(c_ranges));
  nu_ar_exc = malloc(sizeof(double)*(c_ranges));

  for (k = 0; k <= c_ranges; k++) {
    if(k<=10000){
      v = sqrt(2*k*d_epsilon1*e_charge/e_mass);}
      else{
        v = sqrt(2*(10+(k-10000)*d_epsilon2)*e_charge/e_mass);}

        nu_ar_mom[k] = ar_density * sigma_ar_mom[k] * v;
        nu_ar_ion[k] = ar_density * sigma_ar_ion[k] * v;
        nu_ar_exc[k] = ar_density * sigma_ar_exc[k] * v;
        nu_tot[k] = nu_ar_mom[k] + nu_ar_ion[k] + nu_ar_exc[k];

    //the constant collision frequency is the maximizer of the
    //sum of the collision frequecies of all processes
        nu_star= ((nu_star>= nu_tot[k])? nu_star : nu_tot[k]);
      }
}

/****************************/
/* read cross section files */
/****************************/
void load_cs(char *datafile, double *c)
{
  FILE *file = fopen(datafile, "r");
  int i=0;
  float num;
  while(fscanf(file, "%f", &num) > 0) {
    c[i] = num;
    i++;
  }
  fclose(file);

}

/**********************/
/* get cross sections */
/**********************/
void get_cross_sections()
{
  printf("> loading cross sections\n");
  sigma_tot = malloc(sizeof(double)*(c_ranges));
  sigma_ar_mom = malloc(sizeof(double)*(c_ranges));
  sigma_ar_ion = malloc(sizeof(double)*(c_ranges));
  sigma_ar_exc = malloc(sizeof(double)*(c_ranges));
  load_cs("./CS/CS_AR_mom.txt", sigma_ar_mom);
  load_cs("./CS/CS_AR_ion.txt", sigma_ar_ion);
  load_cs("./CS/CS_AR_exc.txt", sigma_ar_exc);

  //printf("> Free memory after init. : %ld\n", memavail());
}

/*********************************/
/* free memory of cross sections */
/*********************************/
void free_sigma()
{
  free(sigma_tot);
  free(sigma_ar_mom);
  free(sigma_ar_ion);
  free(sigma_ar_exc);
}


/**************************************************************************/
/* Put a particle in a given position - x - moving in a certain           */
/* direction - m = cos(theta) - with a certain energy - e - and at time t */
/**************************************************************************/
void put_particle(init_state** part_list, double x,double e,double m, double t)
{
  init_state *h;
  int e_index;
  int x_index;
  int t_index;
  int mu_index;

  h = (init_state *)malloc(sizeof(init_state));
  h->x0 = x;
  h->e0 = e;
  h->m0 = m;
  h->t0 = t;
  h->next = *part_list;
  *part_list = h;

  e_index = (int)(e/d_epsilon2);
  x_index = (int)(x/(max_x/(double)x_ranges));
  if (x_index>= x_ranges){
    printf("You need to increase x range!\n");
    exit(0);
  }
  t_index = (int)(t/(time_simul/(double)t_ranges));
  mu_index = (int)((1+m)/(2/(double)(mu_ranges)));
  //increment on ditribution function
  if (t_index<t_ranges){
    eedf[e_index][t_index][x_index][mu_index]++;
  }
}

/*******************************/
/* Momentum transfer collision */
/*******************************/
void coll_momentum(double atom_mass,int k, struct LOC_collision *LINK)
{
  double theta_c, khi, fi, de;
  int x_index;
  x_index = (int)(*LINK->x/(max_x/(double)x_ranges));

  //increase processes counter
  proc[k][x_index]++;
  theta_c = acos(*LINK->m);

  //isotropic scattering 
  khi = genrand_real1() * M_PI;
  fi = genrand_real1() * 2 * M_PI;
  *LINK->m = *LINK->m * cos(khi) + sin(theta_c) * sin(khi) * cos(fi);
  de = *LINK->e * 2 * e_mass / atom_mass * (1 - cos(khi));
  *LINK->e -= de;

  //decrease in energy 
  power_loss[k] += de;
}

/************************/
/* Excitation collision */
/************************/
void coll_excitation(double level,int k, LOC_collision *LINK)
{
  double fi, khi, theta_c;
  int x_index;
  x_index = (int)(*LINK->x/(max_x/(double)x_ranges));

  if (*LINK->e < level)
    return;
  proc[k][x_index]++;
  *LINK->e -= level;
  theta_c = acos(*LINK->m);
  khi = genrand_real1() * M_PI;
  fi = genrand_real1() * 2 * M_PI;
  *LINK->m = *LINK->m * cos(khi) + sin(theta_c) * sin(khi) * cos(fi);

  //decrease in energy
  power_loss[k] += level;
}

/**********************/
/* Ionizing collision */
/**********************/
void coll_ionization(double ion_level, int k, struct LOC_collision *LINK)
{
  double khi, fi, theta_c, khi2, fi2, m1, m2;
  int x_index;

  if (*LINK->e < ion_level)
    return;
  no_of_e++;
  x_index = (int)(*LINK->x/(max_x/(double)x_ranges));

  proc[k][x_index]++;
  /* equal sharing of energy */
  *LINK->e = (*LINK->e - ion_level) / 2;
  /* isotropic scattering   */
  khi = genrand_real1() * M_PI;
  /*khi:=pi/4*/
  /* equal energy sc. angle */
  fi = genrand_real1() * M_PI * 2;
  theta_c = acos(*LINK->m);
  m1 = *LINK->m * cos(khi) + sin(theta_c) * sin(khi) * cos(fi);
  khi2 = khi;
  fi2 = fi + M_PI;
  m2 = *LINK->m * cos(khi2) + sin(theta_c) * sin(khi2) * cos(fi2);

  //put particle created in the ionization in time out list
  //with the same energy, position and time as the one that
  //created it in the time out list
  put_particle( &f_list, *LINK->x, *LINK->e, m2, *LINK->t);

  *LINK->m = m1;

  //decrease in energy
  power_loss[k] += ion_level;
}

/*************************/
/* Choose collision type */
/*************************/
void collision(double *x_, double *e_, double *m_, double *t_)
{
  struct LOC_collision V;
  short e_index;
  double rnd, t1, t2, t3;

  V.x = x_;
  V.e = e_;
  V.m = m_;
  V.t = t_;

  if(*V.e<10){e_index = (int)(*V.e / d_epsilon1);}
  else e_index = 10000+(int)((*V.e-10)/d_epsilon2);
  if (e_index > c_ranges){e_index = c_ranges;}

  t1 = nu_ar_mom[e_index];
  t2 = t1 + nu_ar_exc[e_index];
  t3 = t2 + nu_ar_ion[e_index];
  rnd = genrand_real1();

  if(rnd> t3/nu_star){ //null collision
    return;
  }
  if (rnd < t1 / nu_star) {
    coll_momentum(ar_mass,0, &V);
    return;
  }
  if (rnd < t2/nu_star) {
    coll_excitation(ar_exc, 2, &V);
    return;
  }
  else{
    coll_ionization(ar_ion_level,1,&V);
    return;
  }
}


/************************************************/
/*Initializes eedf, processes, power array to 0 */
/************************************************/
void initialize(){

  printf("> initializing \n");

  //e_list initializes at NULL;
  //this will make get_particle return null when it reaches the
  //begining of e_list
  e_list = NULL;

  //allocation of memory for distribution function
  eedf = calloc(e_ranges, sizeof(int***));
  
  for(int it_e = 0; it_e < e_ranges; it_e++){
    eedf[it_e] = calloc(t_ranges,sizeof(int **));
    if(eedf[it_e] == NULL){
      fprintf(stderr, "out of memory\n");
      return;
    }
    for(int it_t = 0; it_t < t_ranges;it_t++)	{
      eedf[it_e][it_t] = calloc(x_ranges, sizeof(int *));
      if(eedf[it_e][it_t] == NULL){
       fprintf(stderr, "out of memory\n");
       return;
     }
     for(int it_x = 0; it_x < x_ranges;it_x++) {
       eedf[it_e][it_t][it_x] = calloc(mu_ranges,sizeof(int));
       if(eedf[it_e][it_t][it_x] == NULL){
         fprintf(stderr, "out of memory\n");
         return;
       }
     }
   }
 }

  //allocation of memory for power loss
 for(int i=0; i<processes; i++){
  power_loss[i]=0;
}

  //allocation of memory for processes counter
for(int i=0; i<processes; i++){
  for(int it_x=0; it_x<processes; it_x++){
    proc[i][it_x]=0;
  }
}

}

/*******************************************************************************/
/*Get a particle from particle_list and keep its characteristics at x, e and m */
/*It returns 'false' if there are no particles in particle list                */
/*If there are particles left it gets the current position, 'moves' to         */
/*the next particle and returns true                                           */
/*******************************************************************************/
bool get_particle(init_state **part_list, double *x, double *e, double *m, double *t)
{ 
  init_state *h;
  if (*part_list != NULL) {
    h = *part_list;
    *part_list = h->next;
    *x = h->x0;
    *e = h->e0;
    *t = h->t0;
    *m = h->m0;
    free(h);
    return true;
  }
  else {
    return false;
  }
}


/*************************************************************/
/* advance electrons in time in a homogeneous electric field */
/*************************************************************/
void trace(double *x, double *eps, double *mu, double *t)
//phase space (x, epsilon, mu)
{
  double xe0, ye0, ze0, vxe0, vye0, vze0, t0;
  double xe1, ye1, ze1, vxe1, vye1, vze1;
  double dxe, dye, dze;
  double e_x, v, s1, s2;
  int x_index;
  
  if(*t<time_simul){
    //copy of phase space variables
    xe0 = *x; 
    ye0=0; 
    ze0=0;

    //copy of time
    t0=*t;

    //compute velocities according to energy and angle with x
    v = sqrt(2 * (*eps) * e_charge / e_mass); //velocity
    vxe0 = v * (*mu);                         // v along the E- field
    vye0 = v * sqrt(1 - (*mu) * (*mu));       // v perpend to E- field
    vze0 = 0;
    s1 = e_charge * DT_MC * DT_MC / 2 / e_mass;
    s2 = e_charge * DT_MC / e_mass;   

    //electric field
    e_x = e_field;

    //advance in time
    *t+=DT_MC;
    
    //advance space
    dxe = vxe0 * DT_MC + s1 * e_x;
    dye = vye0 * DT_MC;
    dze = vze0 * DT_MC;

    xe1 = xe0 + dxe;
    ye1 = ye0 + dye;
    ze1 = ze0 + dze;

    //advance momentum
    vxe1 = vxe0 + s2 * e_x;
    vye1 = vye0;
    vze1 = vze0;

    //computing drift velocity
    x_index = (int)(xe0/(max_x/(double)x_ranges));
    v_drift_aux[x_index] += (vxe1+vxe0)/2 * DT_MC;
    t_drift[x_index] += DT_MC;

    //change energy and angle with x according to change in velocity
    *eps = e_mass / 2 / e_charge * (vxe1 * vxe1 + vye1 * vye1 + vze1 * vze1);
    *mu = vxe1 / sqrt(vxe1 * vxe1 + vye1 * vye1 + vze1 * vze1);

    //choose collision event
    collision(&xe1, eps, mu, t);

    //time of the particle
    t0+=DT_MC;
    put_particle(&f_list, xe1, *eps, *mu, t0);
  }
  else //do nothing to this electron
    put_particle(&f_list,*x,*eps,*mu,*t);

}

/********************************************************/
/* emit 1 e-  with energy init_e and cos(theta)=init_mu */
/********************************************************/
void emit_electron(double init_e, double init_mu ){
  double init_x = 0.0;
  double init_t=0;
  
  put_particle(&e_list,init_x, init_e, init_mu, init_t);   /* emit 1 e- */

}

 /***************************/
 /* Normalize and save eedf */
 /***************************/

void save_eedf()
{
  FILE *f;
  double energy;
  double position;
  double aux_time;
  double mu;
  char STR1[256];
  char f_NAME[200];
  double sum;

  for(int it_e=0; it_e<e_ranges; it_e++){
    for (int it_t = 0; it_t<t_ranges ; it_t++){
      for (int it_x = 0; it_x<x_ranges; it_x++){
       for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
         total_num_e += eedf[it_e][it_t][it_x][it_mu];
       }
     }
   }
 }

  //normalized distribution of electrons as a function of energy
 sprintf(STR1, "./output/%s-eedf.dat", run);
 strcpy(f_NAME, STR1);
 f = fopen(f_NAME, "w");
 if (f == NULL)
  printf("File to save eedf not found");

fprintf(f, "# e(eV)  eedf(eV^(-3/2))\n"); 
for (int it_e = 0; it_e < e_ranges; it_e++) {
  energy = (it_e+0.5)* d_epsilon2 ;
  sum = 0;
  for (int it_t = 0; it_t<t_ranges ; it_t++){
    for (int it_x = 0; it_x<x_ranges; it_x++){
     for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
       sum +=eedf[it_e][it_t][it_x][it_mu];
     }
   }
 }
 fprintf(f, "%12.3f  % .5E\n", energy, sum/total_num_e/d_epsilon2/sqrt(energy)); 
}
fclose(f);

  //normalized distribution of electrons in space
sprintf(STR1, "./output/%s-ex.dat", run);
strcpy(f_NAME, STR1);
f = fopen(f_NAME, "w");
if (f == NULL)
  printf("File to save eedf not found");

fprintf(f, "# x(m)  α(number of electrons)\n"); 
for (int it_x = 0; it_x<x_ranges; it_x++){
  position = (it_x+0.5)* max_x/(double)(x_ranges) ;
  sum = 0;
  for (int it_e = 0; it_e < e_ranges; it_e++) {
    for (int it_t = 0; it_t<t_ranges ; it_t++){
     for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
       sum +=eedf[it_e][it_t][it_x][it_mu];
     }
   }
 }
 fprintf(f, "%12.3f  % .5E\n", position, sum/total_num_e/(max_x/(double)(x_ranges))); 
}
fclose(f);


  //normalized radial distribition of electrons
sprintf(STR1, "./output/%s-emu.dat", run);
strcpy(f_NAME, STR1);
f = fopen(f_NAME, "w");
if (f == NULL)
  printf("File to save eedf not found");

fprintf(f, "# cos(theta)  α(number of electrons)\n");

for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
  mu = (it_mu+0.5)*(2/(double)(mu_ranges))-1;
  sum = 0;
  for (int it_e = 0; it_e < e_ranges; it_e++){
    for (int it_t = 0; it_t<t_ranges ; it_t++){
     for (int it_x = 0; it_x<x_ranges; it_x++){
       sum +=eedf[it_e][it_t][it_x][it_mu];
     }
   }
 }
 fprintf(f, "%12.3f  % .5E\n", mu, sum/total_num_e/(2/(double)(mu_ranges))); 
}
fclose(f);


  //normalized distribution of electrons in time
sprintf(STR1, "./output/%s-etime.dat", run);
strcpy(f_NAME, STR1);
f = fopen(f_NAME, "w");
if (f == NULL)
  printf("File to save eedf not found");

fprintf(f, "# t(s)  α(number of electrons)\n"); 
for (int it_t = 0; it_t<t_ranges ; it_t++){
  aux_time = (it_t+0.5)*(time_simul/(double)(t_ranges));
  sum = 0;
  for (int it_e = 0; it_e < e_ranges; it_e++){
    for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
     for (int it_x = 0; it_x<x_ranges; it_x++){
       sum +=eedf[it_e][it_t][it_x][it_mu];
     }
   }
 }
 fprintf(f, "%.3E  % .5E\n", aux_time, sum/total_num_e/(time_simul/(double)(t_ranges))); 
}
fclose(f);


  //distribution of electrons in space for each time
sprintf(STR1, "./output/%s-ext.dat", run);
strcpy(f_NAME, STR1);
f = fopen(f_NAME, "w");
if (f == NULL)
  printf("File to save eedf not found");
for (int it_t = 0; it_t<t_ranges ; it_t++){
  fprintf(f, "# t = %.3E \n\n", (it_t+0.5)*time_simul/(double)(t_ranges));
  for (int it_x = 0; it_x<x_ranges; it_x++){
    position = (it_x+0.5)* max_x/(double)(x_ranges) ;
    sum = 0;
    for (int it_e = 0; it_e < e_ranges; it_e++) {
     for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
       sum +=eedf[it_e][it_t][it_x][it_mu];
     }
   }
   fprintf(f, "%.3E\t%.5E\n", position, sum/total_num_e); 
 }
 fprintf(f,"\n");
}
fclose(f);
}


void rate_coeffs(){
  printf(">> Calculating rate coeffs \n");
  double energy=0;
  double sum=0;
  int k=0;

  FILE *f1,*f2,*f3;
  char f1_NAME[200];
  char f2_NAME[200];
  char f3_NAME[200];

  sprintf(f1_NAME, "./output/%s-ratecoeffs-kmom.dat", run);
  sprintf(f2_NAME, "./output/%s-ratecoeffs-kexc.dat", run);
  sprintf(f3_NAME, "./output/%s-ratecoeffs-kion.dat", run);

  f1 = fopen(f1_NAME, "w");
  f2 = fopen(f2_NAME, "w");
  f3 = fopen(f3_NAME, "w");

  if (f1 == NULL)
    printf("File to save eedf not found");
  if (f2 == NULL)
    printf("File to save eedf not found");
  if (f3 == NULL)
    printf("File to save eedf not found");
  
  fprintf(f1, "# energy(ev) \t K_mom (m^3/s)\n"); 
  fprintf(f2, "# energy(ev) \t K_exc (m^3/s)\n"); 
  fprintf(f3, "# energy(ev) \t K_ion (m^3/s)\n"); 

  for (int it_e = 0; it_e <e_ranges; it_e++) {
    sum=0;
    energy = (it_e+0.5)* d_epsilon2 ;
  //printf("Energy %.4E\n",energy );
    for (int it_t = 0; it_t<t_ranges ; it_t++){
      for (int it_x = 0; it_x<x_ranges; it_x++){
       for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
        sum+=eedf[it_e][it_t][it_x][it_mu];
      }
    }
  }
  sum=sum/total_num_e/d_epsilon2/sqrt(energy);

  if(it_e<=100){
    k=it_e*d_epsilon2/d_epsilon1;
    k_mom +=sum*d_epsilon2*energy*sigma_ar_mom[k];
    k_exc +=sum*d_epsilon2*energy*sigma_ar_exc[k];
    k_ion +=sum*d_epsilon2*energy*sigma_ar_ion[k];
  }                                                        
  else
  {
    k_mom +=sum*d_epsilon2*energy*sigma_ar_mom[it_e+10000-100];
    k_exc +=sum*d_epsilon2*energy*sigma_ar_exc[it_e+10000-100];
    k_ion +=sum*d_epsilon2*energy*sigma_ar_ion[it_e+10000-100];
  }

  fprintf(f1,"%.4E \t %.4E \n",energy,k_mom*sqrt(2*e_charge/e_mass)); 
  fprintf(f2,"%.4E \t %.4E \n",energy,k_exc*sqrt(2*e_charge/e_mass)); 
  fprintf(f3,"%.4E \t %.4E \n",energy,k_ion*sqrt(2*e_charge/e_mass)); 

}

  k_mom=k_mom*sqrt(2*e_charge/e_mass); 
  k_exc=k_exc*sqrt(2*e_charge/e_mass); 
  k_ion=k_ion*sqrt(2*e_charge/e_mass); 
}


void e_mean_energy(){
  printf(">> Calculating energy \n");
  double energy=0;
  double sum=0;

  for (int it_e = 0; it_e <e_ranges; it_e++) {
    sum=0;
    energy = (it_e+0.5)* d_epsilon2 ;
  //printf("Energy %.4E\n",energy );
    for (int it_t = 0; it_t<t_ranges ; it_t++){
      for (int it_x = 0; it_x<x_ranges; it_x++){
       for (int it_mu = 0; it_mu<mu_ranges; it_mu++){
        sum+=eedf[it_e][it_t][it_x][it_mu];
      }
    }
  }
  sum=sum/total_num_e/d_epsilon2/sqrt(energy);
  mean_energy+=energy*sqrt(energy)*sum*d_epsilon2;
  }
temp_electron=2/((double)3)*mean_energy;

//printf("Energy \t %.4E\nElectron temperature %.4E \n",mean_energy, temp_electron);

}

/***********************/
/* Free memory of eedf */
/***********************/
void free_eedf(){
  for (int it_e = 0; it_e < e_ranges; ++it_e){
    for(int it_t = 0; it_t < t_ranges; ++it_t){
      for(int it_x= 0; it_x < x_ranges; ++it_x){
        free(eedf[it_e][it_t][it_x]);
      }
      free(eedf[it_e][it_t]);
    }
    free(eedf[it_e]);
  }
  free(eedf);
}

/***********************************/
/* Compute and save drift velocity */
/***********************************/
void drift_velocity(){
  printf(">> Calculating drift velocity \n");
  double v_drift_aux_sum=0.0;
  double t_drift_sum=0.0;
  for (int it_x = 0; it_x < x_ranges; it_x++){
    v_drift_aux_sum += v_drift_aux[it_x];
    t_drift_sum += t_drift[it_x];
  }
  v_drift_summed = v_drift_aux_sum/t_drift_sum;
  
}


/************************************/
/* compute and save townsend coeff */
/***********************************/


void townsend_coeff(){

}


/************************/
/* print summary of run */
/************************/
void print_summary(){
  FILE *f;
  char STR1[256];
  char f_NAME[200];

  sprintf(STR1, "./output/%s-summary.dat", run);
  strcpy(f_NAME, STR1);
  f = fopen(f_NAME, "w");
  if (f == NULL)
    printf("File to save eedf not found");
  fprintf(f,"-------------------------------- %s ---------------------------------\n\n",
    run);
  fprintf(f,"*** Electron transport in Ar plasma ***\n\n");
  fprintf(f,"E / n           = %12.4f   Td\n", e_field / ar_density * 1e21);
  fprintf(f,"Pressure        = %12.2f mbar\n", pressure / 100);
  fprintf(f,"Temperature     = %12.2f    K\n", temperature);
  fprintf(f,"Time limit      = % .5E    s\n", time_simul);
  fprintf(f,"Mean Energy     = % .4E    eV\n", mean_energy); 
  fprintf(f,"Electron Temp   = % .4E    eV\n", temp_electron);


  //integrate processes in x
  double proc0sum = 0, proc1sum = 0,proc2sum = 0;
  for(int it_x = 0; it_x<x_ranges;it_x++){
    proc0sum+= proc[0][it_x];
    proc1sum+= proc[1][it_x];
    proc2sum+= proc[2][it_x];   
  }
  fprintf(f,"Drift Velocity  = %.5E   m/s\n", v_drift_summed);
  fprintf(f,"\n\n");

  
  fprintf(f,"        #Collisions  %% of total   Collision freq.(s-1)  Rate coeff.(m3/s)  Fractional power loss()   α/N(m2)  \n\n");

  fprintf(f, "MOMTR : %.4E%12.2f       % .5E\t\t%.5E \t\t\t  %.4E\n",
   proc0sum, (double)proc0sum * 100/(proc0sum+proc1sum+proc2sum), proc0sum/time_simul, k_mom,
   power_loss[0] / (power_loss[0]+power_loss[1]+ power_loss[2]));
  fprintf(f, "ION   : %.4E%12.2f       % .5E\t\t%.5E \t %17.4f \t\t\t\n",
   proc1sum, (double)proc1sum * 100/(proc0sum+proc1sum+proc2sum), proc1sum/time_simul, k_ion,
   power_loss[1] / (power_loss[0]+power_loss[1]+ power_loss[2]));
  fprintf(f, "EXC   : %.4E%12.2f       % .5E\t\t%.5E \t %17.4f \n",
   proc2sum, (double)proc2sum * 100/(proc0sum+proc1sum+proc2sum), proc2sum/time_simul, k_exc,
   power_loss[2] / (power_loss[0]+power_loss[1]+ power_loss[2]));
  fprintf(f,"----------------------------------------------------------------------\n");
}


/*******************************************************************************/
/******************************* MAIN RUN **************************************/
/*******************************************************************************/
int main(){
  //seed for random number generation
  init_genrand(10);
  //name of the run
  strcpy(run,"RUN1");

  //working conditions
  double ar_percent = 100.0;

  double tot_density = pressure / k_boltz / temperature;
  ar_density = ar_percent / 100 * tot_density;
  e_field = reduced_field * tot_density;
  double r;
  double x=0, e=0, m=0, t=0;
  double t_count=0;
  double cathode_T = 10;  //cathode will emit electrons with 10 eV mean energy
  for (int i=0; i<x_ranges; ++i)
  {
    t_drift[i]=0;
    v_drift_aux[i]=0;
  }


  //cross section loading
  get_cross_sections();
  conv_macroscopic();
  
  //initialization
  initialize();
  
  //cathode emits a maxwellian distribution of electrons
  no_of_e = 1000;

  //sample from maxwellian distribution
  double e_i=0, m_i=0, r_i =0;
  int i = 0;
  
  printf("> emiting electrons \n");
  while(i < no_of_e){
    r_i = genrand_real1();
    e_i = gsl_sf_lambert_W0((double)(2/(double)cathode_T/cathode_T/pi/r_i/r_i))/2*cathode_T;
    //generate angle randomly
    //forward direction
    m_i = cos(genrand_real1()*pi/2);
    emit_electron(e_i,m_i);
    i++;
  }

  //each electrons will be put in e_list
  //we iterate on that list advancing all electrons
  //untill all of them reach the simulation time
  printf("> electrons are being simulated\n");
  while(t_count < time_simul){
    f_list = NULL;
    t_count=time_simul;
    while (get_particle(&e_list,&x,&e,&m,&t)){
      r = -log(genrand_real1());
      DT_MC =1/nu_star*r;
      trace( &x, &e, &m, &t);
      //t_count holds the smallest curent time of all electrons
      //i.e. all electrons have advanced to t_count
      t_count=(t_count<t)? t_count : t;	
    }
    //printf("time: %.4E\n", t_count);
    e_list = f_list;
  }
  printf("> end of simulation\n");
  printf("> generating output files\n");

  //compute and save eedf 
  save_eedf();

  //compute and save rate coefficients (not tested)
  rate_coeffs();

  //compute and save drift velocity
  drift_velocity();
  //compute and save townsend coefficient (not tested)
  //townsend_coeff();

  e_mean_energy();

  print_summary();
  
  printf("> freeing memory\n");
  //free memory
  free_eedf();
  free_sigma();
  
  return 0;
};

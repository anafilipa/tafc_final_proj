set terminal png size 1200,1000 enhanced font 'Verdana,10'

set output 'eedf.png'

set xlabel "energy (eV)"
set xrange[0:0.5]

set yrange[0:1]
set ylabel "eedf (ev^3&+/2)"

set title "eedf"

set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 1.5   # --- blue
plot './CS/CS_AR_mom.dat' with linespoints ls 1